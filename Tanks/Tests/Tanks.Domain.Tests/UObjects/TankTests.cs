﻿using NUnit.Framework;
using System;

namespace Tanks.Domain.UObjects.Tests
{
    [TestFixture]
    public class TankTests
    {
        private Tank Create() => new Tank();

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [TestCase("prop1", "valu1")]
        [TestCase("prop2", -100)]
        [TestCase("prop3", 23.4)]
        [TestCase("prop4", new string[]{"value1", "value2", "value3"})]
        public void SetIndexer_SendedValueAreEqualExpected_True(string property, object value)
        {
            var tank = Create();
            tank[property] = value;

            Assert.That(tank[property], Is.EqualTo(value));
        }

        [TestCase("prop1", "valu1", "newValue")]
        [TestCase("prop2", -100, "NewValue")]
        [TestCase("prop3", 23.4, -2332.4)]
        [TestCase("prop4", new string[] { "value1", "value2", "value3" }, null)]
        public void SetIndexer_ReplaceOldValue_True(string property, object OldValue, object NewValue)
        {
            var tank = Create();
            tank[property] = OldValue;
            tank[property] = NewValue;

            Assert.That(tank[property], Is.EqualTo(NewValue));
        }

        [TestCase("prop1", "valu1")]
        public void GetIndexer_SendedNotExistingPropertyName_ArgumentException(string property, object value)
        {
            var tank = Create();
            tank[property] = value;

            Assert.That(() => _ = tank["NotExistProperty"], Throws.TypeOf<ArgumentException>());
        }

        [TestCase("prop")]
        [TestCase("prop3")]
        [TestCase("NotExistProperty")]
        [TestCase("Some complex property name")]
        public void GetIndexerErrorMessage_SendedNotExistingPropertyName_ArgumentException(string property)
        {
            var tank = Create();
            tank["prop1"] = "value1";

            Assert.That(() => _ = tank[property], Throws.TypeOf<ArgumentException>().With.Message.EqualTo($"Tank haven't property \'{property}\' (Parameter \'property\')"));
        }
    }
}
