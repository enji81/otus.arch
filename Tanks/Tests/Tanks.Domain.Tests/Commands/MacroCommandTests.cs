﻿using Moq;
using NUnit.Framework;
using System;
using Tanks.Domain.Adapters;
using Tanks.Domain.Interfaces;
using Tanks.Domain.UObjects;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class MacroCommandTests
    {
        private MacroCommand Create(ITanksCommand[] commands = null)
        {
            commands ??= new ITanksCommand[] 
            { 
                new MoveCommand(new Mock<IMovable>().Object),
                new RotateCommand(new Mock<IRotable>().Object)
            };
            return new MacroCommand(commands);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void Execute_AreAllCommandExecuted_True()
        {
            var command1 = new Mock<ITanksCommand>();
            command1.Setup(x => x.Execute()).Verifiable();

            var command2 = new Mock<ITanksCommand>();
            command2.Setup(x => x.Execute()).Verifiable();

            MacroCommand macro = Create(new ITanksCommand[]{ command1.Object, command2.Object});
            macro.Execute();

            command1.Verify(x => x.Execute(), Times.Once);
            command2.Verify(x => x.Execute(), Times.Once);
        }
       
        [Test]
        public void Execute_FueldNotEnough_Exception()
        {
            Tank tank = new Tank();
            tank["Position"] = new Vector(12, 5);
            tank["Velocity"] = new Vector(-7, 5);
            tank["Fuel"] = 90;
            tank["FuelForStep"] = 100;

            var move = new MoveCommand(new MovableAdapter(tank));
            var fuelable = new FuelableAdapter(tank);
            var check = new CheckFuelCommand(fuelable);
            var burn = new BurnFuelCommand(fuelable);
            var macro = new MacroCommand(check,burn, move);

            Assert.Throws<Exception>(() => macro.Execute());
        }

    }
}
