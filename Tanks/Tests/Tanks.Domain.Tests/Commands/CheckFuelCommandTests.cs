﻿using Moq;
using NUnit.Framework;
using System;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class CheckFuelCommandTests
    {
        private CheckFuelCommand Create(IFuelable fuelable = null)
        {
            if (fuelable == null)
            {
                var mock = new Mock<IFuelable>();
                mock.Setup(x => x.Fuel).Returns(100);
                mock.Setup(x => x.FuelForStep).Returns(10);
                fuelable = mock.Object;
            }
            return new CheckFuelCommand(fuelable);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [TestCase(100, 100)]
        [TestCase(100,10)]
        public void Check_FuelIsEnougForMove_NotException(int fuel, int fuelStep)
        {
            var mock = new Mock<IFuelable>();
            mock.Setup(x => x.Fuel).Returns(fuel);
            mock.Setup(x => x.FuelForStep).Returns(fuelStep);

            var check = Create(mock.Object);
            Assert.DoesNotThrow(() => check.Execute());
        }

        [TestCase(100, 110)]
        [TestCase(1, 10)]
        public void Check_FuelIsNotEnoughForMove_Exception(int fuel, int fuelStep)
        {
            var mock = new Mock<IFuelable>();
            mock.Setup(x => x.Fuel).Returns(fuel);
            mock.Setup(x => x.FuelForStep).Returns(fuelStep);

            var check = Create(mock.Object);
            Assert.Throws<Exception>(() => check.Execute());
        }
    }
}
