﻿using Moq;
using NUnit.Framework;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class RotateCommandTests
    {
        private RotateCommand Create(IRotable rotable = null)
        {
            if (rotable == null)
            {
                var mock = new Mock<IRotable>();
                mock.Setup(x => x.AngularVelocity).Returns(10);
                mock.Setup(x => x.AngularVelocity).Returns(2);
                mock.Setup(x => x.MaxDirections).Returns(8);
                rotable = mock.Object;
            }
            return new RotateCommand(rotable);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [TestCase(2, 3, 8, 5)]
        [TestCase(2, 3, 8, 5)]
        [TestCase(3, 2, 8, 5)]
        [TestCase(-2, 3,8, 1)]
        [TestCase(11, -2, 8, 1)]
        [TestCase(8, 2, 8, 2)]
        [TestCase(-2, 11,8, 1)]
        [TestCase(2, -11, 8,1)]
        public void Rotate_NewDirectionAreEqualExpected_True(int direction, int velocity, int max, int expected)
        {
            var rotable = new Mock<IRotable>();
            rotable.Setup(x => x.Direction).Returns(direction);
            rotable.Setup(x => x.AngularVelocity).Returns(velocity);
            rotable.Setup(x => x.MaxDirections).Returns(max);
            rotable.SetupSet(x => x.Direction = expected).Verifiable();
            var rotate = Create(rotable.Object);
            rotate.Execute();

            rotable.Verify();
        }
    }
}
