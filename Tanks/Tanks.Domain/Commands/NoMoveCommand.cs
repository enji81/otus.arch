﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class NoMoveCommand : ITanksCommand
    {
        public NoMoveCommand(IMovable movable){}
        public void Execute() { }
    }
}
