﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class MacroCommand : ITanksCommand
    {
        private readonly ITanksCommand[] commands;
        public MacroCommand(params ITanksCommand[] commands)
        {
            this.commands = commands;
        }
        public void Execute()
        {
            foreach (ITanksCommand command in commands)
            {
                command.Execute();
            }
        }
    }
}
