﻿namespace Tanks.Domain.Interfaces
{
    public interface ITanksCommand
    {
        void Execute();
    }
}
