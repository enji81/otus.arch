﻿namespace Tanks.Domain
{
    public class Vector
    {
        private readonly int[] body;
        public Vector(params int[] body)
        {
            this.body = body;
        }

        public static Vector operator + (Vector v1, Vector v2)
        {
            int[] newBody = new int[v1.body.Length];
            for(int i = 0; i< newBody.Length; i++)
            {
                newBody[i] = v1.body[i] + v2.body[i];
            }
            return new Vector(newBody);
        }
        public static bool operator ==(Vector v1, Vector v2)
        {
            if (object.ReferenceEquals(v1, v2)) return true;
            if (object.ReferenceEquals(v1,null) || object.ReferenceEquals(v2,null)) return false;
            for (int i = 0; i < v1.body.Length; i++)
            {
                if (v1.body[i] != v2.body[i]) return false;
            }
            return true;
        }
        
        public static bool operator !=(Vector v1, Vector v2) => !(v1 == v2);
        public override int GetHashCode() => body.GetHashCode();
        public override bool Equals(object obj)
        {
            if (!(obj is Vector other)) return false;

            if (body.Length != other.body.Length) return false;
            for (int i = 0; i < body.Length; i++)
            {
                if (body[i] != other.body[i]) return false;
            }
            return true;
        }
    }
}
