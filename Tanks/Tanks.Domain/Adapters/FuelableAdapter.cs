﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Adapters
{
    public class FuelableAdapter : IFuelable
    {
        private readonly IUObject uObject;
        public FuelableAdapter(IUObject uObject)
        {
            this.uObject = uObject;
        }
        public int Fuel 
        { 
            get => (int)uObject["Fuel"]; 
            set => uObject["Fuel"] = value; 
        }

        public int FuelForStep => (int)uObject["FuelForStep"]; 
    }
}
